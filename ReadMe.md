# Debian Packaging for Provengo

This project creates a Debian package from Provengo uber-jar. 
It was tested on Ubuntu 22.04 and might need some adjustments to run on other versions of Ubuntu, Debian or Debian derivatives.

## Prerequisites

Install build dependencies

`sudo apt install build-essential devscripts debhelper dh-make`

## Quick start

0. [Download](https://downloads.provengo.tech/downloads.html) jar and save in upstreem folder as `provengo.jar`.
0. Run the build script.
0. Install Debian package found in `local` folder, or uplod it to upstreem server.
