#!/bin/bash

export DEBEMAIL="dev@provengo.tech"
export UPSTREAM_VERSION="1.0"
export CHANGELOG_MSG="Initial release. (Closes: #XXXXXX)"

mkdir --parents local
rm -rf local/*
cd local

mkdir provengo-${UPSTREAM_VERSION}
cp ../upstreem/provengo.jar provengo-${UPSTREAM_VERSION}/
cp ../resources/provengo.sh provengo-${UPSTREAM_VERSION}/
cp ../resources/Makefile provengo-${UPSTREAM_VERSION}/
tar -czf provengo_${UPSTREAM_VERSION}.orig.tar.gz provengo-${UPSTREAM_VERSION}

cd provengo-${UPSTREAM_VERSION}
mkdir --parents debian/source
dch --create -v ${UPSTREAM_VERSION}-1 --package provengo $CHANGELOG_MSG
cp ../../resources/deb/copyright debian/
cp ../../resources/deb/control debian/
cp ../../resources/deb/rules debian/
cp ../../resources/deb/provengo.dirs debian/
cp ../../resources/deb/source/format debian/source/

debuild -us -uc
